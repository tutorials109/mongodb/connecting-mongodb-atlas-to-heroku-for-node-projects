- [References](#references)
- [Deploying Backend with Heroku](#deploying-backend-with-heroku)
  - [Prerequisites](#prerequisites)
    - [Installing Heroku CLI](#installing-heroku-cli)
      - [Ubuntu](#ubuntu)
      - [NPM](#npm)
      - [Confirm your installation](#confirm-your-installation)
    - [Login](#login)
  - [Deployment](#deployment)
    - [Heroku Remote](#heroku-remote)
    - [Pushing code to Heroku](#pushing-code-to-heroku)
    - [Update `Package.json` scripts](#update-packagejson-scripts)
    - [Setting buildpack](#setting-buildpack)
    - [Setting config variables](#setting-config-variables)

# References
- https://devcenter.heroku.com/articles/heroku-cli#install-the-heroku-cli
- https://devcenter.heroku.com/articles/heroku-cli-commands
- https://devcenter.heroku.com/articles/git
- https://devcenter.heroku.com/articles/buildpacks
- https://devcenter.heroku.com/articles/config-vars
- https://dashboard.heroku.com/apps
- https://docs.google.com/document/d/1De7-yMGjzcsCb1_lsoZRiKQ0kGhnWG0aYKYc9yDYXPk/edit
- https://git-scm.com/

# Deploying Backend with Heroku

## Prerequisites
1. [Install Git](https://git-scm.com/)
2. [Install Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli#install-the-heroku-cli)


### Installing Heroku CLI

#### Ubuntu
Version will not autoupdate. Update it manually with `apt-get`.

```sh
curl https://cli-assets.heroku.com/install-ubuntu.sh | sh
```

#### NPM
This is the least recommended option.

> It’s strongly recommended to use one of the other installation methods if possible.
>
> This installation method doesn’t autoupdate. It also requires you to use your system’s version of Node.js, which can be older than the version Heroku develops the CLI against. Heroku uses current releases of Node.js and doesn’t support older versions.
>
> If you use any of the other installation methods, it includes the proper version of Node.js and doesn’t conflict with any other version on your system.
>
> Also, this method doesn’t use the yarn lockfile for dependencies like the others do, even if you install with yarn. This method can cause issues if the CLI’s dependencies become incompatible in minor or patch releases.

Run the command: 

```sh
npm install -g heroku
```

#### Confirm your installation

```sh
heroku --version

# command output
heroku/7.62.0 wsl-x64 node-v16.15.0
```

### Login

Run the command :

```sh
heroku login
```

You'll then be prompted to login through the Heroku website. Once you login on the website it should automatically log you in on your terminal. You can close the browser page.

If you prefer to login through the terminal you can use `heroku login -i`.

```sh
# output after logging in
Logged in as dominik@gmail.com
```

> The CLI saves your email address and an API token to ~/.netrc for future use.

## Deployment
Before you can deploy your app to Heroku, initialize a local Git repository and commit your application code to it.

```sh
git init
git add .
git commit -m '<commit message>'
```

### Heroku Remote
We're now going to set up a Heroku remote. We'll do this by running this command :

```sh
# syntax
heroku create -a <app name>

# example
heroku create -a gorillaz-api-ilja
```

Running the above command will give us this output. The first is our application URL and the second is the applications remote git repository for Heroku.

```sh
# command output
https://gorillaz-api-ilja.herokuapp.com/ | https://git.heroku.com/gorillaz-api-ilja.git

# application url
https://gorillaz-api-ilja.herokuapp.com/

# heroku git repository
https://git.heroku.com/gorillaz-api-ilja.git
```

If we run the `git remote -v` command we'll see our remote repo.

```sh
# command output
heroku  https://git.heroku.com/gorillaz-api-ilja.git (fetch)
heroku  https://git.heroku.com/gorillaz-api-ilja.git (push)
```

### Pushing code to Heroku
In the above output we see `heroku https://git.heroku.com/gorillaz-api-ilja.git (push)`. `heroku` is the name of the remote repository. To push our code to this branch we'll run :

```sh
# use master if your local branch is named msater or main if it is named main
git push heroku main or git push heroku master
```

Use this same command whenever you want to deploy the latest committed version of your code to Heroku.

Heroku only deploys code that you push to the master or main branches of the remote. Pushing code to another branch of the heroku remote has no effect.

```sh
# Command Output
ilja@wsl:~/.../gorillaz-api$ git push heroku master
Enumerating objects: 60, done.
Counting objects: 100% (60/60), done.
Delta compression using up to 16 threads
Compressing objects: 100% (52/52), done.
Writing objects: 100% (60/60), 472.25 KiB | 9.64 MiB/s, done.
Total 60 (delta 8), reused 0 (delta 0)
remote: Compressing source files... done.
remote: Building source:
remote: 
remote: -----> Building on the Heroku-20 stack
remote: -----> Determining which buildpack to use for this app
remote: -----> Node.js app detected
remote:        
remote: -----> Creating runtime environment
remote:        
remote:        NPM_CONFIG_LOGLEVEL=error
remote:        USE_YARN_CACHE=true
remote:        NODE_VERBOSE=false
remote:        NODE_ENV=production
remote:        NODE_MODULES_CACHE=true
remote:        
remote: -----> Installing binaries
remote:        engines.node (package.json):  unspecified
remote:        engines.npm (package.json):   unspecified (use default)
remote:        engines.yarn (package.json):  unspecified (use default)
remote:        
remote:        Resolving node version 16.x...
remote:        Downloading and installing node 16.16.0...
remote:        Using default npm version: 8.11.0
remote:        Resolving yarn version 1.22.x...
remote:        Downloading and installing yarn (1.22.19)
remote:        Installed yarn 1.22.19
remote:        
remote: -----> Installing dependencies
remote:        Installing node modules (yarn.lock)
remote:        yarn install v1.22.19
remote:        [1/4] Resolving packages...
remote:        [2/4] Fetching packages...
remote:        [3/4] Linking dependencies...
remote:        [4/4] Building fresh packages...
remote:        Done in 2.26s.
remote:        
remote: -----> Build
remote:        
remote: -----> Pruning devDependencies
remote:        yarn install v1.22.19
remote:        [1/4] Resolving packages...
remote:        [2/4] Fetching packages...
remote:        [3/4] Linking dependencies...
remote:        [4/4] Building fresh packages...
remote:        warning Ignored scripts due to flag.
remote:        Done in 0.40s.
remote:        
remote: -----> Caching build
remote:        - yarn cache
remote:        
remote: -----> Build succeeded!
remote: -----> Discovering process types
remote:        Procfile declares types     -> (none)
remote:        Default types for buildpack -> web
remote: 
remote: -----> Compressing...
remote:        Done: 36.2M
remote: -----> Launching...
remote:        Released v3
remote:        https://gorillaz-api-ilja.herokuapp.com/ deployed to Heroku
remote: 
remote: This app is using the Heroku-20 stack, however a newer stack is available.
remote: To upgrade to Heroku-22, see:
remote: https://devcenter.heroku.com/articles/upgrading-to-the-latest-stack
remote: 
remote: Verifying deploy... done.
To https://git.heroku.com/gorillaz-api-ilja.git
 * [new branch]      master -> master
```

### Update `Package.json` scripts

Add the `start` script to your `Package.json`. This script is run by heroku. It starts up our server.

```json
"scripts": {
  "start": "node server.js",
}
```

### Setting buildpack

> Buildpacks are responsible for transforming deployed code into a slug, which can then be executed on a dyno. Buildpacks are composed of a set of scripts, and depending on the programming language, the scripts will retrieve dependencies, output generated assets or compiled code, and more. This output is assembled into a slug by the slug compiler.

To set a buildpack for our heroku project we run the command :

```sh
# set build pack
heroku buildpacks:set heroku/nodejs

# check buildpacks being used
heroku buildpacks

# command output
=== gorillaz-api-ilja Buildpack URL
heroku/nodejs
```

This is setting out buildpack to work with `node.js` since our project is written in JavaScript.

### Setting config variables

We're going to setup variables to allow heroku to use our MongoDB Atlas URI.

```sh
# syntax
heroku config:set <variable name>="<variable value>"

# example 
heroku config:set PROD_MONGODB="mongodb+srv://BigDuck:123456@c-lust-r.abc465.mongodb.net/database?retryWrites=true&w=majority"

# command output
=== gorillaz-api-ilja Config Vars
PROD_MONGODB: mongodb+srv://BigDuck:123456@c-lust-r.abc465.mongodb.net/database?retryWrites=true&w=majority
```

This is then referenced by our database in `db/index.js` :

```js
const mongoose = require('mongoose');
require('dotenv').config();

const LOCAL_URI = 'mongodb://127.0.0.1:27017/gorillazDatabase';
const MONGODB_URI = process.env.PROD_MONGODB || process.env.MONGODB_URI || LOCAL_URI;

```
Notice how `PROD_MONGODB` matches the end of `process.env.PROD_MONGODB`. This variable is set in heroku when we do `heroku config:set PROD_MONGODB="..."`. After setting it Node will then be able to reference it. Therefore connecting MongoDB atlas with Heroku!
